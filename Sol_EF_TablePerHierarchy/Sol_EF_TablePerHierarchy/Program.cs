﻿using Sol_EF_Table_Per_HIerarchy_Inheritance.Entity;
using Sol_EF_TablePerHierarchy.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_TablePerHierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<EmployeeEntity> listEmployeeObj =
                   await new EmployeeRepository().GetEmployeeData();



                IEnumerable<VendorEntity> listVendorObj =
                    await new VendorRepository().GetVendorData();

            }).Wait();
        }
    }
}
