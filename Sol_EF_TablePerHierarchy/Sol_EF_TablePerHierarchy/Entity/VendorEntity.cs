﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_Table_Per_HIerarchy_Inheritance.Entity
{
    public class VendorEntity : PersonEntity
    {
        public decimal? Wages { get; set; }
    }
}
