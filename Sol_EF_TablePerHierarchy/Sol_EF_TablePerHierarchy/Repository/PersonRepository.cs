﻿using Sol_EF_TablePerHierarchy.EF;

namespace Sol_EF_TablePerHierarchy.Repository
{
    public class PersonRepository
    {
        #region Declaration
        private PersondbEntities personDbEnties = null;
        #endregion

        #region Constructor
        public PersonRepository()
        {
            personDbEnties = new PersondbEntities();

            PersonDbEntites = personDbEnties;
        }
        #endregion

        #region Public Property
        public PersondbEntities PersonDbEntites { get; set; }
        #endregion


    }
}
