﻿using Sol_EF_Table_Per_HIerarchy_Inheritance.Entity;
using Sol_EF_TablePerHierarchy.EF;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sol_EF_TablePerHierarchy.Repository
{
    public class VendorRepository : PersonRepository
    {

        #region Constructor
        public VendorRepository() : base()
        {

        }
        #endregion 

        #region Public Method
        public async Task<IEnumerable<VendorEntity>> GetVendorData()
        {
            return await Task.Run(() => {

                var getQuery =
                    base.PersonDbEntites
                    ?.tblPersonAlls
                    ?.OfType<Vendor>()
                    ?.AsEnumerable()
                    ?.Select((leVendorObj) => new VendorEntity()
                    {
                        FirstName = leVendorObj.FirstName,
                        LastName = leVendorObj.LastName,
                        PersonId = leVendorObj.PersonId,
                        Wages = leVendorObj.Wages
                    })
                    ?.ToList();

                return getQuery;

            });
        }
        #endregion 
    }
}
